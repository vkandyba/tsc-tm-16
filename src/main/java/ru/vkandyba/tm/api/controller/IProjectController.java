package ru.vkandyba.tm.api.controller;

import ru.vkandyba.tm.model.Project;

public interface IProjectController {

    void showById();

    void removeById();

    void removeByName();

    void removeByIndex();

    void updateById();

    void updateByIndex();

    void showProject(Project project);

    void showByIndex();

    void showByName();

    void showProjects();

    void clearProjects();

    void createProject();

    void startById();

    void startByIndex();

    void startByName();

    void finishById();

    void finishByIndex();

    void finishByName();

    void changeStatusById();

    void changeStatusByIndex();

    void changeStatusByName();

}
