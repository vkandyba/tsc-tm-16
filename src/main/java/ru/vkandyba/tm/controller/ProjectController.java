package ru.vkandyba.tm.controller;

import ru.vkandyba.tm.api.controller.IProjectController;
import ru.vkandyba.tm.api.service.IProjectService;
import ru.vkandyba.tm.enumerated.Sort;
import ru.vkandyba.tm.enumerated.Status;
import ru.vkandyba.tm.exception.entity.ProjectNotFoundException;
import ru.vkandyba.tm.model.Project;
import ru.vkandyba.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class ProjectController implements IProjectController {

    private IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showById() {
        System.out.println("Enter Id");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findById(id);
        if (project == null) {
            throw new ProjectNotFoundException();
        }
        showProject(project);
    }

    @Override
    public void removeById() {
        System.out.println("Enter Id");
        final String id = TerminalUtil.nextLine();
        projectService.removeById(id);
        System.out.println("[OK]");
    }

    @Override
    public void removeByName() {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        projectService.removeByName(name);
        System.out.println("[OK]");
    }

    @Override
    public void removeByIndex() {
        System.out.println("Enter Index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        projectService.removeByIndex(index);
        System.out.println("[OK]");
    }

    @Override
    public void updateById() {
        System.out.println("Enter Id");
        final String id = TerminalUtil.nextLine();
        if (projectService.existsById(id)) {
            throw new ProjectNotFoundException();
        }
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.updateById(id, name, description);
        if (project == null) {
            throw new ProjectNotFoundException();
        } else {
            System.out.println("[OK]");
        }
    }

    @Override
    public void updateByIndex() {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        if (!projectService.existsByIndex(index)) {
            throw new ProjectNotFoundException();
        }
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.updateByIndex(index, name, description);
        if (project == null) {
            throw new ProjectNotFoundException();
        } else {
            System.out.println("[OK]");
        }
    }

    @Override
    public void showProject(Project project) {
        final List<Project> projects = projectService.findAll();
        System.out.println("Index: " + projects.indexOf(project));
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus());
    }

    @Override
    public void showByIndex() {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        if (!projectService.existsByIndex(index)) {
            throw new ProjectNotFoundException();
        }
        final Project project = projectService.findByIndex(index);
        showProject(project);
    }

    @Override
    public void showByName() {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findByName(name);
        if (project == null) {
            throw new ProjectNotFoundException();
        }
        showProject(project);
    }

    @Override
    public void showProjects() {
        System.out.println("[LIST PROJECTS]");
        System.out.println("Enter sort");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();

        List<Project> projects;
        if(sort == null || sort.isEmpty()) projects = projectService.findAll();
        else{
            Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            projects = projectService.findAll(sortType.getComparator());
        }
        for (Project project : projects) {
            System.out.println(projects.indexOf(project) + 1 + ". " + project.getName() + " " + project.getId() + ": " + project.getDescription());
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearProjects() {
        System.out.println("[CLEAR PROJECTS]");
        projectService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        projectService.create(name, description);
        System.out.println("[OK]");
    }

    @Override
    public void startById() {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.startById(id);
        if(project==null) throw new ProjectNotFoundException();
    }

    @Override
    public void startByIndex() {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.startByIndex(index);
        if(project==null) throw new ProjectNotFoundException();

    }

    @Override
    public void startByName() {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.startByName(name);
        if(project==null) throw new ProjectNotFoundException();

    }

    @Override
    public void finishById() {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.finishById(id);
        if(project==null) throw new ProjectNotFoundException();
    }

    @Override
    public void finishByIndex() {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.finishByIndex(index);
        if(project==null) throw new ProjectNotFoundException();

    }

    @Override
    public void finishByName() {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.finishByName(name);
        if(project==null) throw new ProjectNotFoundException();
    }

    @Override
    public void changeStatusById() {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        System.out.println("Enter status");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Project project = projectService.changeStatusById(id, status);
        if(project==null) throw new ProjectNotFoundException();

    }

    @Override
    public void changeStatusByIndex() {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("Enter status");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Project project = projectService.changeStatusByIndex(index, status);
        if(project==null) throw new ProjectNotFoundException();

    }

    @Override
    public void changeStatusByName() {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter status");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Project project = projectService.changeStatusByName(name, status);
        if(project==null) throw new ProjectNotFoundException();
    }

}
