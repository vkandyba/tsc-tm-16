package ru.vkandyba.tm.controller;

import ru.vkandyba.tm.api.controller.ICommandController;
import ru.vkandyba.tm.api.service.ICommandService;
import ru.vkandyba.tm.model.Command;
import ru.vkandyba.tm.util.NumberUtil;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    public void showErrorArgument() {
        System.err.println("Error! Argument not supported...");
    }

    public void showErrorCommand() {
        System.err.println("Error! Command not found...");
    }

    @Override
    public void exit() {
        System.exit(0);
    }

    @Override
    public void showInfo() {
        System.out.println("[INFO]");
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + NumberUtil.formatBytes(availableProcessors));
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = (maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue);
        System.out.println("Maximum memory: " + maxMemoryFormat);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory: " + NumberUtil.formatBytes(usedMemory));
    }

    @Override
    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("DEVELOPER: Vyachesav Kandyba");
        System.out.println("E-MAIL: vkandyba@tsconsulting.com");
    }

    @Override
    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    @Override
    public void showCommands() {
        System.out.println("[COMMANDS]");
        final Command[] commands = commandService.getCommands();
        for (final Command command : commands) showCommandValue(command.getName());
    }

    @Override
    public void showArguments() {
        System.out.println("[ARGUMENTS]");
        final Command[] commands = commandService.getCommands();
        for (final Command command : commands) showCommandValue(command.getArgument());
    }

    private void showCommandValue(final String value) {
        if (value == null || value.isEmpty()) return;
        System.out.println(value);
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getCommands();
        for (final Command command : commands) {
            System.out.println(command);
        }
    }

}
